# Geração de Assinatura XML

Este é exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java. 

Este exemplo apresenta os passos necessários para a geração de assinatura XML utilizando-se chave privada armazenada em disco.
  - Passo 1: Carregamento da chave privada e conteúdo do certificado digital.
  - Passo 2: Inicialização da assinatura e produção dos artefatos signedAttributes.
  - Passo 3: Cifragem dos dados inicializados com o certificado armazenado em disco.
  - Passo 4: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas Java abaixo:
* [BouncyCastle] - A Java Crypto API!
* [FasterXML] - FasterXML Jackson library.
* [RestAssured] - Testing and validating REST client in Java
* [JDK 8] - Java 8

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a localização deste arquivo no computador, bem como a senha para acessar seu conteúdo.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| INSERT_VALID_ACCESS_TOKEN | Access Token para o consumo do serviço (JWT). | ServiceConfig
| LOCATION_OF_PRIVATE_KEY_FILE | Localização do arquivo da chave privada a ser configurada na aplicação. | CertificateConfig
| PRIVATE_KEY_PASSWORD | Senha do arquivo da chave privada a ser configurada na aplicação. | CertificateConfig

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, compile diretamente as classes ou importe o projeto em sua IDE de preferência.
Utilizamos o JRE versão 8 para desenvolvimento e execução.



   [RestAssured]: <http://rest-assured.io/>
   [BouncyCastle]: <https://www.bouncycastle.org/>
   [FasterXML]: <http://fasterxml.com>
   [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>

