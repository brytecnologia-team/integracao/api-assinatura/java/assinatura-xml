package br.com.bry.framework.sample.util;

import java.io.File;

public class PKCS1DTO {
	
	private String nonce;
	private String signedAttribute;
	private String signatureValue;
	private String initializedDocument;
	private File originalDocument;
	
	public PKCS1DTO(String nonce, String signedAttribute, String initializedDocument) {
		this.nonce = nonce;
		this.signedAttribute = signedAttribute;
		this.initializedDocument = initializedDocument;
	}
	
	public void setSignatureValue(String signatureValue) {
		this.signatureValue = signatureValue;
	}
	
	public void setOriginalDocument(File originalDocument) {
		this.originalDocument = originalDocument;
	}
	
	public String getNonce() {
		return nonce;
	}
	
	public String getSignedAttribute() {
		return signedAttribute;
	}
	
	public String getSignatureValue() {
		return signatureValue;
	}
	
	public File getOriginalDocument() {
		return originalDocument;
	}

	public String getInitializedDocument() { return initializedDocument; }

}
