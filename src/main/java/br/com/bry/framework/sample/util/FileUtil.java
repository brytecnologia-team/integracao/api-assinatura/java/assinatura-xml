package br.com.bry.framework.sample.util;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import br.com.bry.framework.sample.config.SignatureConfig;

public class FileUtil {
	
	public static void writeContentToFile(String filePath, String fileName, String content) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("_yyyy-MM-dd hh.mm.ss.SSS");

		String signatureFullName = filePath + fileName + formatter.format(new Date(System.currentTimeMillis())) + ".xml";
		
		FileOutputStream in = new FileOutputStream(new File(signatureFullName));
		in.write(Base64.getDecoder().decode(content));
		in.close();

		System.out.println("Successful signature generation: " + SignatureConfig.OUTPUT_RESOURCE_FOLDER + signatureFullName);
		
	}
	
}
