package br.com.bry.framework.sample.config;

public class ServiceConfig {
	
	private final static String URL_SERVER = "https://fw2.bry.com.br";
	
	private final static String URL_SERVER_CMS = URL_SERVER + "/api/xml-signature-service/v1/signatures/";
	
	public static final String URL_INITIALIZE_SIGNATURE = URL_SERVER_CMS + "initialize";
	
	public static final String URL_FINALIZE_SIGNATURE = URL_SERVER_CMS + "finalize";
	
	public static final String ACCESS_TOKEN = "<INSERT_VALID_ACCESS_TOKEN>";
	
}
