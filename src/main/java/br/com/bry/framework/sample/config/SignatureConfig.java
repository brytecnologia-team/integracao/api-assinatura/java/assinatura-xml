package br.com.bry.framework.sample.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SignatureConfig {

	private static final String PATH_SEPARATOR = File.separator;
	
	private static final String RESOURCES_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src" + PATH_SEPARATOR
	        + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR;
	
	public static final String OUTPUT_RESOURCE_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src" + PATH_SEPARATOR
	        + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "generatedSignatures" + PATH_SEPARATOR;
	
	// Available values: "true" and "false".
	public static final String ATTACHED = "true";

	// Available values: "BASIC", "CHAIN", "CHAIN_CRL", "TIMESTAMP", "COMPLETE", "ADRB", "ADRT", "ADRV", "ADRC", "ADRA", "ETSI_B", "ETSI_T", "ETSI_LT" and "ETSI_LTA".
	public static final String PROFILE = "BASIC";

	// Available values: ENVELOPED, ENVELOPING and DETACHED.
	public static final String SIGNATURE_FORMAT = "ENVELOPED";

	// Available values: "SHA1", "SHA256" e "SHA512".
	public static final String HASH_ALGORITHM = "SHA256";

	// Available values: "SIGNATURE", "CO_SIGNATURE	" and "COUNTER_SIGNATURE".
	public static final String OPERATION_TYPE = "SIGNATURE";
	
	public static final List<String> ORIGINAL_DOCUMENTS_LOCATION = new ArrayList<>();
	
	static {
		ORIGINAL_DOCUMENTS_LOCATION.add(RESOURCES_FOLDER + "documents" + PATH_SEPARATOR + "sample.xml");
	}
	
}
