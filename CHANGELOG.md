# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
 
Empty

## [1.0.2] - 2020-10-15

### Fixed
#### README.
- Fixed accepted values by basic request attribute attached (Correct values: *true* and *false*).
 
[1.0.2]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/assinatura-cms/-/tags/1.0.2

## [1.0.1] - 2020-04-01

### Fixed
#### README.
- LOCATION_OF_PRIVATE_KEY_FILE - Updated configuration class reference.
- PRIVATE_KEY_PASSWORD - Updated configuration class reference.
 
[1.0.1]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/assinatura-cms/-/tags/1.0.1

## [1.0.0] - 2020-03-03

### Added

- A SIGNATURE generation example.
- README
- CHANGELOG
- LICENSE

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/assinatura-xml/-/tags/1.0.0

